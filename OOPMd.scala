/**
 * Created by Matiss Zervens on 2015.11.12..
 */


class Primes(val limit: Int)
{
  private val numList = (4 to limit).toList
  var primeList = List(3, 2)

  if(limit <= 1)
    primeList = List[Int]()
  else if(limit == 2)
    primeList = List(2)
  else if(limit > 3)
    genPrimes()

  def isPrime(num: Int): Boolean =
  {
    primeList.contains(num)
  }

  private def genPrimes() =
  {
    for(num <- numList)
    {
      var isPrime = true
      for{prime <- primeList
          if(prime < num)
          if((num % prime) == 0)
      }
      {
        isPrime = false
      }

      if(isPrime)
        primeList = num :: primeList
    }

  }

}

/**
 * Uzdevuma formulējums:
 *  S304. Sastādīt Scala programmu, kas dotam pozitīvam veselam skaitlim n atrod visus tādus
 *        pozitīvu veselu skaitļu pārus i un j, kur 1 <= i < j < n, ka i + j ir pirmskaitlis.
 *        Risinājumā lietot for-ietveršanas (for-comprehension) konstrukciju.
 */
class OOPMd(val n: Int)
{
  private val nList = (1 to n).toList
  private val prime = new Primes(n + (n - 1))

  def findPairs(): List[(Int, Int)] =
  {
    for{
      i <- nList
      j <- nList
      if (i >= 1 && i < j && j < n && prime.isPrime(i + j))
    }
      yield (i, j)
  }

}


object tester
{
  def main (args: Array[String]): Unit =
  {
    val pairFinder = new OOPMd(13)
    val pairs = pairFinder.findPairs()
    pairs.foreach(pair => print(pair.toString() + " "))

  }

}

